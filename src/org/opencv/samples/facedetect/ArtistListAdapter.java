package org.opencv.samples.facedetect;

import java.util.List;

import android.app.Activity;
import android.graphics.Color;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;

public class ArtistListAdapter implements OnItemClickListener {

	int listId;
	ListView list;
	ListView list2;
	Activity activity;

	public ArtistListAdapter(Activity activity, int listId, ListView artistListA, ListView artistListB){
		this.listId = listId;
		this.list = artistListA;
		this.activity = activity;
		this.list2 = artistListB;
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

		for(int i = 0; i < list.getCount(); i++){
			View v = list.getChildAt(i);
			v.setBackgroundColor(Color.BLACK);
		}
		
		for(int i = 0; i < list2.getCount(); i++){
			View v = list2.getChildAt(i);
			v.setBackgroundColor(Color.BLACK);
		}
		
		Artist artist = Scott.getArtist(Scott.ARTIST_LIST_A,position);
		view.setBackgroundColor(Color.parseColor("#FF8800"));

		TextView name = (TextView) activity.findViewById(R.id.artist_name);
		name.setText(artist.getName());

		Scott.activeArtistPosition = position;
		Scott.activeArtistList = listId;

	}

}
