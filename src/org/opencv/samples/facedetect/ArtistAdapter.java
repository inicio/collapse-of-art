package org.opencv.samples.facedetect;

import java.util.List;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

public class ArtistAdapter extends ArrayAdapter<Artist> {

    private Typeface font;
    private List<Artist> items;
    private Context context;

    public ArtistAdapter(Context context, int textViewResourceId, List<Artist> objects) {
        super(context, textViewResourceId, objects);
        this.items = objects;
        this.context = context;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        LayoutInflater vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = vi.inflate(R.layout.artists_list_item, null);

        ImageView profile = (ImageView) v.findViewById(R.id.artist_profile);
        profile.setImageResource(items.get(position).getProfile());
        
        // We'll have to set/update a value

        return v;
    }
}
