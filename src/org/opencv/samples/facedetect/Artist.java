package org.opencv.samples.facedetect;

import android.content.Context;
import android.os.Message;

public class Artist {
	
	Context context;
	
	Artwork artwork;
	String imageUrl;
	String name;
	
	int list;
	
	int valuation = 1;
	
	public Artist(String imageUrl, String name, String artworkTitle) {
		this.imageUrl = imageUrl;
		this.name = name;
		this.artwork = new Artwork(artworkTitle);
	}
	
	public Artist(String imageUrl, String name) {
		this.imageUrl = imageUrl;
		this.name = name;
	}
	
	private class Artwork {
		
		String title;
		int totalFaces;
		int currentFaces;
		
		public Artwork(String title){
			this.title = title;
		}
		
		public String getTitle(){
			return title;
		}
		
		public void setTotalFaces(int totalFaces){
//			Message msg = new Message();
			//String textTochange = "text";
			//msg.obj = textTochange;
			//Main.mHandler.sendMessage(msg);
			this.totalFaces = totalFaces;
		}
		
		public void setCurrentFaces(int currentFaces) {
			this.currentFaces = currentFaces;
		}
		
	}
	
	// -- Setters
	
	public void setValuation(int valuation){
		this.valuation = valuation;
	}
	
	public void setContext(Context context) {
		this.context = context;
	}
	
	public void setList(int i){
		list = i;
	}
	
	// -- Getters
	
	public int getProfile(){
        return context.getResources().getIdentifier(
        		imageUrl, 
        		"drawable" , 
        		context.getPackageName()); 
	}

	public String getName(){
		return name;
	}
	
	public String getArtworkTitle(){
		return artwork.getTitle();
	}
	
	public int getValuation(){
		return valuation;
	}
	
	public int getList(){
		return list;
	}
}
