package org.opencv.samples.facedetect;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import org.apache.http.client.ClientProtocolException;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewFrame;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewListener2;
import org.opencv.android.Utils;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfRect;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.objdetect.CascadeClassifier;

import com.google.gson.JsonSyntaxException;

import android.graphics.Bitmap;
import android.util.Log;

public class CameraListener implements CvCameraViewListener2 {

	private static final Scalar    FACE_RECT_COLOR     = new Scalar(255, 138, 0, 255);
	private static final Scalar    TRANS_WHITE         = new Scalar(255, 255, 255, 50);
	private static final Scalar	   BLACK               = new Scalar(0,0,0,255);
	private static final Scalar    BLACK_TRANS	= new Scalar(0, 0, 0, 50);

	private Mat                    mRgba;
	private Mat                    mGray;

	private float                  mRelativeFaceSize   = 0.2f;
	private int                    mAbsoluteFaceSize   = 0;

	private CascadeClassifier      mJavaDetector;

	private List<Integer> faceTime;

	private int artistIndex = 0;

	public CameraListener (CascadeClassifier mJavaDetector) {
		this.mJavaDetector = mJavaDetector;	
	}

	public void setRelativeFaceSize(float relativeFaceSize) {
		this.mRelativeFaceSize = relativeFaceSize;
	}

	public void setAbsoluteFaceSize(int absoluteFaceSize) {
		this.mAbsoluteFaceSize = absoluteFaceSize;
	}

	public void setDetector(CascadeClassifier detector) {
		this.mJavaDetector = detector;
	}

	public void onCameraViewStarted(int width, int height) {
		mGray = new Mat();
		mRgba = new Mat();
		faceTime = new ArrayList<Integer>();
		faceTime.add(1); // start us with something
	}

	public void onCameraViewStopped() {
		mGray.release();
		mRgba.release();
	}

	public Mat onCameraFrame(CvCameraViewFrame inputFrame) {

		mRgba = inputFrame.rgba();
		mGray = inputFrame.gray();

		if (mAbsoluteFaceSize == 0) {
			int height = mGray.rows();
			if (Math.round(height * mRelativeFaceSize) > 0) {
				mAbsoluteFaceSize = Math.round(height * mRelativeFaceSize);
			}
		}

		MatOfRect faces = new MatOfRect();

		if (mJavaDetector != null)
			mJavaDetector.detectMultiScale(mGray, faces, 1.1, 2, 2, // TODO: objdetect.CV_HAAR_SCALE_IMAGE
					new Size(mAbsoluteFaceSize, mAbsoluteFaceSize), new Size());

		// RESET some things if the artist changes
		if(artistIndex != Scott.activeArtistPosition) {
			faceTime.clear();
			artistIndex = Scott.activeArtistPosition;
		}

		// Clip the image to make it look consistent
		Core.rectangle(mRgba, new Point(0,0), new Point(10,720), BLACK, -1);
		Core.rectangle(mRgba, new Point(950,0), new Point(960,720), BLACK, -1);
				
		//Core.rectangle(mRgba, new Point(0,0), new Point(960,130), BLACK_TRANS, -1);

		// CAN WE DO FANCY STUFF HERE?
		
		// Bitmap (nothing yet)
		Bitmap bitmap = Bitmap.createBitmap(mRgba.width(), mRgba.height(), Bitmap.Config.ARGB_8888);
		Utils.matToBitmap(mRgba, bitmap);

		// Mat (nothing yet)
		Utils.bitmapToMat(bitmap, mRgba);

		Rect[] facesArray = faces.toArray();
		for (int x = 0; x < mRgba.cols(); x+=20){
			Core.line(mRgba, new Point(x,0), new Point(x, 720), BLACK,1);
		}
		for(int y = 0; y < mRgba.rows(); y+=20){ 
			Core.line(mRgba, new Point(0,y), new Point(960, y), BLACK,1);
		}
				
		//		Core.rectangle(mRgba, new Point(x,y), new Point(x+20, y+ 20), BLACK, 1);
				
				/*
				for (int i = 0; i < facesArray.length; i++){
					if(facesArray[i].tl().x >= x && facesArray[i].br().x <= x
							&& facesArray[i].tl().y >= y && facesArray[i].br().y <= y)
						Core.rectangle(mRgba, new Point(x,y), new Point(x+20, y+ 20), FACE_RECT_COLOR, -1);
				}
				*/
				
		
		
		for (int i = 0; i < facesArray.length; i++) {
			Core.rectangle(mRgba, facesArray[i].tl(), facesArray[i].br(), FACE_RECT_COLOR, 1);

			if(i < faceTime.size()){
				int t = faceTime.get(i) + 1;
				faceTime.set(i, t);
			} else if (i >= faceTime.size()){
				faceTime.add(1);
			}

			Core.putText(mRgba, faceTime.get(i) + " (millis)", new Point(facesArray[i].br().x + 15, facesArray[i].br().y), 0, 1, FACE_RECT_COLOR, 2);

		}

		
		
		Scott.appendConsoleText("Scott");

		// Why did I buy a Student Important Portrait
		// Dot matrix style computer vision?
		// Complexity. Overwhelming information

		// Show Face Count
		// Core.putText(mRgba, "Ppl: " + facesArray.length, new Point(0d,50d), 0, 2, FACE_RECT_COLOR,2);

		// Update Artist Valuation
		Map<String, String> factors = new HashMap<String, String>();
		factors.put("faces", String.valueOf(facesArray.length));
		int value = Evaluator.getValue(Scott.getActiveArtist(), factors);
		Scott.getActiveArtist().setValuation(value);

		return mRgba;
	}

}
