package org.opencv.samples.facedetect;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.widget.TextView;

public class Scott extends Application {

    public static final String TAG = "OCVSample::Activity";
    private static Context context;
    public static String consoleText;
    
    public final static int ARTIST_LIST_A = 0;
    public final static int ARTIST_LIST_B = 1;
    
    // List of Artists
    public static int activeArtistPosition = 0;
    public static int activeArtistList = 0;
    
    public static List<Artist> artistsA;
    public static List<Artist> artistsB;
    static {

    	artistsA = new ArrayList<Artist>();
    	artistsB = new ArrayList<Artist>();
    	
    	artistsA.add(new Artist("i_004", "Alex Jordan", "At Risk"));
		artistsA.add(new Artist("i_005", "Amanda Wood", "Do Not Disturb"));
		artistsA.add(new Artist("i_006", "Cristal Sung", "Aspire"));
			
    	artistsB.add(new Artist("i_007", "EMILY HALBERSTADT", "Essence"));
    	artistsB.add(new Artist("i_008", "Dean Park", "Living Within Decay"));
		artistsB.add(new Artist("i_009", "Erin Sweeney", "Living Within Decay"));
	
		artistsA.add(new Artist("i_010", "Jamie Chirico", "Cereal Questions"));
		artistsA.add(new Artist("i_011", "Ji Woong You", "Parallel Play"));
		artistsB.add(new Artist("i_012", "KAREN COCHRANE", "Play On"));
	
		artistsB.add(new Artist("i_013", "OLIVIA KOLAKOWSKI", "Play On"));
		//artistsB.add(new Artist("i_014", "KATHRYN BARRETT", "A World Left Over"));
		
	
	
		
    }
       
    @Override
    public void onCreate() {
        super.onCreate();
        Scott.context = getApplicationContext();
    }

    public static Artist getArtist(int list, int position){
    	if(list == ARTIST_LIST_A)
    		return artistsA.get(position);
    	else
    		return artistsB.get(position);
    }
    
    public static Context getAppContext() {
        return Scott.context;
    }
    
    public static Artist getActiveArtist(){
    	return Scott.getArtist(activeArtistList,activeArtistPosition);
    }
    
    public static void appendConsoleText(String t){
    	consoleText = "\n" + consoleText + "\n" + t;
    }
    
    public static String getConsoleText(){
    	String text = Scott.consoleText;
    	//Scott.consoleText = "";
    	return text;
    }
    

}
