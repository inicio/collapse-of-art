package org.opencv.samples.facedetect;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import com.google.gson.JsonSyntaxException;

public class TickerDraw extends SurfaceView implements Runnable {

	Thread thread = null;
	SurfaceHolder surfaceHolder;
	volatile boolean running = false;

	private Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
	Random random;

	List<Ticker> tickers;

	public TickerDraw(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
		surfaceHolder = getHolder();
		
		random = new Random();

		// Set the stocks
		// Make our Ticker List
		tickers = new ArrayList<Ticker>();
		new Runnable() {
			int position = 0;
			@Override
			public void run(){
				for(HashMap<String,String> hash : Stock.stocks) {
					try {
						//Stock stock = Stock.getStock(hash.get("exchange"), hash.get("ticker"));
						Stock stock = new Stock();
						tickers.add(new Ticker(stock, position++));
					} catch (JsonSyntaxException e) {

					}
				}
			}
		}.run();

	}

	public void onResumeMySurfaceView(){
		running = true;
		thread = new Thread(this);
		thread.start();
	}

	public void onPauseMySurfaceView(){
		boolean retry = true;
		running = false;
		while(retry){
			try {
				thread.join();
				retry = false;
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	@Override
	public void run() {

		Canvas c;
		while (running) {
			c = null;
			try {
				c = surfaceHolder.lockCanvas(null);
				synchronized (surfaceHolder) {
					

					// Show Stocks... as tickers
					for(Ticker t : tickers){
						t.render(c);
					}
					
					draw(c);

				}
			} finally {
				// do this in a finally so that if an exception is thrown
				// during the above, we don't leave the Surface in an
				// inconsistent state
				if (c != null) {
					surfaceHolder.unlockCanvasAndPost(c);
				}
			}
		}

	}


}
