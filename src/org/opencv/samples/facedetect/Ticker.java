package org.opencv.samples.facedetect;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Scalar;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

public class Ticker {

	private static int TICKER_LENGTH = 960;
	
	private Stock stock;
	private int position;
	
	int x;
	int y;
	
	int width = 100;
	int height;
	
	
	
	private static final Scalar    TICKER_TEXT     = new Scalar(0, 255, 0, 255);
	
	public Ticker(Stock stock, int position) {
		this.stock = stock;
		this.position = position;
		x = 0;
		y = 300;
		setStartPosition();
	}
	
	public void setStartPosition(){
		x = position * width;
	}
	
	public void render(Canvas canvas) {
		
		Paint paint = new Paint();
		paint.setColor(Color.GREEN);
		paint.setTextSize(20.0f);
		
		canvas.drawText("TICKER", x += 5, y, paint);
		if (x > 960) x = -width;
		
	}
	
	public void render(Mat mRgba) {
		
		Core.putText(
				mRgba, 
				"TICKER", 
				new Point(x,y), 
				0, 
				2, 
				TICKER_TEXT);
		
		// if
		x += 10;
		
		if(x > 960) x = -width;
		
		
	}
	
}
