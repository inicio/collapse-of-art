package org.opencv.samples.facedetect;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.util.Log;
import android.view.TextureView;

public class TickerThread extends Thread {
    private final TextureView mSurface;
    private volatile boolean mRunning = true;

    public TickerThread(TextureView surface) {
        mSurface = surface;
    }

    @Override
    public void run() {
        float x = 0.0f;
        float y = 0.0f;
        float speedX = 5.0f;
        float speedY = 3.0f;
       
        Paint paint = new Paint();
        paint.setColor(0xff00ff00);

        while (mRunning && !Thread.interrupted()) {
            final Canvas canvas = mSurface.lockCanvas(null);
            try {
                canvas.drawColor(0xff00ff00, PorterDuff.Mode.CLEAR);
                canvas.drawRect(x, y, x + 20.0f, y + 20.0f, paint);
            } finally {
                mSurface.unlockCanvasAndPost(canvas);
            }

            if (x + 20.0f + speedX >= mSurface.getWidth() || x + speedX <= 0.0f) {
                speedX = -speedX;
            }
            if (y + 20.0f + speedY >= mSurface.getHeight() || y + speedY <= 0.0f) {
                speedY = -speedY;
            }

            x += speedX;
            y += speedY;

            try {
                Thread.sleep(15);
            } catch (InterruptedException e) {
                // Interrupted
            }
            
            Log.d("SCOTT","display.. come on");
            
        }
    }
    
    void stopRendering() {
        interrupt();
        mRunning = false;
    }
    
}