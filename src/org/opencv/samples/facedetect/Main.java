package org.opencv.samples.facedetect;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraBridgeViewBase;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.objdetect.CascadeClassifier;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SurfaceView;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

public class Main extends Activity {

	private static final String    TAG                 = "OCVSample::Activity";
	public static final int        JAVA_DETECTOR       = 0;

	private MenuItem               mItemFace50;
	private MenuItem               mItemFace40;
	private MenuItem               mItemFace30;
	private MenuItem               mItemFace20;
	private MenuItem               mItemType;

	private File                   mCascadeFile;
	private CascadeClassifier      mJavaDetector;

	private int                    mDetectorType       = JAVA_DETECTOR;
	private String[]               mDetectorName;

	private CameraBridgeViewBase   mOpenCvCameraView;
	private CameraListener 		   cameraListener;

	public static Activity activity;

	private BaseLoaderCallback  mLoaderCallback = new BaseLoaderCallback(this) {
		@Override
		public void onManagerConnected(int status) {
			switch (status) {
			case LoaderCallbackInterface.SUCCESS: 

				System.loadLibrary("detection_based_tracker");

				try {
					// load cascade file from application resources
					InputStream is = getResources().openRawResource(R.raw.lbpcascade_frontalface);
					File cascadeDir = getDir("cascade", Context.MODE_PRIVATE);
					mCascadeFile = new File(cascadeDir, "lbpcascade_frontalface.xml");
					FileOutputStream os = new FileOutputStream(mCascadeFile);

					byte[] buffer = new byte[4096];
					int bytesRead;
					while ((bytesRead = is.read(buffer)) != -1) {
						os.write(buffer, 0, bytesRead);
					}
					is.close();
					os.close();

					mJavaDetector = new CascadeClassifier(mCascadeFile.getAbsolutePath());
					if (mJavaDetector.empty()) {
						Log.e(TAG, "Failed to load cascade classifier");
						mJavaDetector = null;
					}

					cascadeDir.delete();

					if(cameraListener == null)
						cameraListener = new CameraListener(mJavaDetector);
					else
						cameraListener.setDetector(mJavaDetector);

				} catch (IOException e) {
					e.printStackTrace();
					Log.e(TAG, "Failed to load cascade. Exception thrown: " + e);
				}

				mOpenCvCameraView.enableView();
				break;
			default:
				super.onManagerConnected(status);
				break;
			}
		}
	};

	public Main() {
		mDetectorName = new String[2];
		mDetectorName[JAVA_DETECTOR] = "Java";
	}

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.main);
		Main.activity = this;
		
		// Start Stock Monitor
		Stock stockHelper = new Stock();
		stockHelper.startMonitor(this);

		// Start Information Helper
		InformationHelper info = new InformationHelper();
		info.startUpdater(this);

		// Start console
		console();

		// Validate Detector
		if(cameraListener == null)
			cameraListener = new CameraListener(mJavaDetector);
		else
			cameraListener.setDetector(mJavaDetector);

		// Setup Camera View
		mOpenCvCameraView = (CameraBridgeViewBase) findViewById(R.id.opencv);
		mOpenCvCameraView.setVisibility(SurfaceView.VISIBLE);
		mOpenCvCameraView.setCvCameraViewListener(cameraListener);
		setMinFaceSize(0.2f);

		// Setup List of Artists
		for(Artist a : Scott.artistsA){
			a.setContext(this);
		}
		for(Artist a : Scott.artistsB){
			a.setContext(this);
		}

		// List A
		ArtistAdapter adapterA = new ArtistAdapter(this, R.layout.artists_list_item, Scott.artistsA);	
		ListView artistListA = (ListView) findViewById(R.id.artist_list_A);
		artistListA.setAdapter(adapterA);
		artistListA.setDividerHeight(0);
		
		// List B
		ArtistAdapter adapterB = new ArtistAdapter(this, R.layout.artists_list_item, Scott.artistsB);
		ListView artistListB = (ListView) findViewById(R.id.artist_list_B);
		artistListB.setAdapter(adapterB);
		artistListB.setDividerHeight(0);

		final ListView _listA = artistListA;
		final ListView _listB = artistListB;
		
		ArtistListAdapter aa = new ArtistListAdapter(this,Scott.ARTIST_LIST_A,artistListA, artistListB);
		ArtistListAdapter bb = new ArtistListAdapter(this,Scott.ARTIST_LIST_B,artistListB, artistListA);
		artistListA.setOnItemClickListener(aa);
		artistListB.setOnItemClickListener(bb);

		// Setup ticker Canvas
		TickerDraw tickerDraw = new TickerDraw(this);
		tickerDraw.run();

	}

	@Override
	public void onPause()
	{
		super.onPause();
		if (mOpenCvCameraView != null)
			mOpenCvCameraView.disableView();
	}

	@Override
	public void onResume()
	{
		super.onResume();
		OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_2_4_3, this, mLoaderCallback);
	}

	public void onDestroy() {
		super.onDestroy();
		mOpenCvCameraView.disableView();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		Log.i(TAG, "called onCreateOptionsMenu");
		mItemFace50 = menu.add("Face size 50%");
		mItemFace40 = menu.add("Face size 40%");
		mItemFace30 = menu.add("Face size 30%");
		mItemFace20 = menu.add("Face size 20%");
		mItemType   = menu.add(mDetectorName[mDetectorType]);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		Log.i(TAG, "called onOptionsItemSelected; selected item: " + item);
		if (item == mItemFace50)
			setMinFaceSize(0.5f);
		else if (item == mItemFace40)
			setMinFaceSize(0.4f);
		else if (item == mItemFace30)
			setMinFaceSize(0.3f);
		else if (item == mItemFace20)
			setMinFaceSize(0.2f);
		else if (item == mItemType) {
			mDetectorType = (mDetectorType + 1) % mDetectorName.length;
			item.setTitle(mDetectorName[mDetectorType]);
			setDetectorType(mDetectorType);
		}
		return true;
	}

	private void setMinFaceSize(float faceSize) {
		cameraListener.setRelativeFaceSize(faceSize);
		cameraListener.setAbsoluteFaceSize(0);
	}

	private void setDetectorType(int type) {
		if (mDetectorType != type) {
			mDetectorType = type;
		}
	}

	public void console(){

		new Thread (new Runnable() {
			private final Handler mHandler = new Handler();
			private int line = 0;

			@Override
			public void run(){
				Scott.appendConsoleText((line++) + " collapseofart$: ");
				String text = Scott.getConsoleText();
				TextView console = (TextView) findViewById(R.id.console);
				Log.d("CONSOLE", "console set to: " + text);
				console.setText(text);
				mHandler.postDelayed(this, 500);
			}

		}).start();

	}

}