package org.opencv.samples.facedetect;

import java.util.Map;

import android.os.Message;


public class Evaluator {

	public static int getValue(Artist artist, Map factors){
		
		int faces = 1;
		
		int value = 0;
		int currentValue = artist.getValuation();
		
		// faces
		if (factors.containsKey("faces"))
			faces = Integer.parseInt(factors.get("faces").toString());
		
		value = currentValue + faces;
		
		Scott.appendConsoleText(artist.getArtworkTitle());
				
		return value;
	}
	
}
