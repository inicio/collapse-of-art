package org.opencv.samples.facedetect;

import android.app.Activity;
import android.os.Handler;
import android.util.Log;
import android.widget.TextView;

/**
 * This Class is going to keep all of the GUI info up to date!
 * @author josh
 *
 */

public class InformationHelper {

	public void startUpdater(Activity _activity){

		final Activity activity = _activity;

		new Thread (new Runnable() {

		    private final Handler mHandler = new Handler();


		    @Override
		    public void run() {
		    	
		    	Artist artist = Scott.getActiveArtist();

				TextView artworkValuation = (TextView) activity.findViewById(R.id.artwork_valuation);

				Log.d("SCOTT", "value is - "  + artist.getValuation());

				artworkValuation.setText("$ " + String.valueOf(artist.getValuation()) + " $");				
				// Stock tickers need to scroll (might do this in CameraListener (?)
		    	
		        mHandler.postDelayed(this, 100);       
		    }
		    
		}).start();

	}
}
