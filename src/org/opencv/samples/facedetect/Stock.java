package org.opencv.samples.facedetect;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ExecutionException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
 class Stock {

	String id;
	String t; // ticker
	String e;
	String l; // price
	String l_cur;
	String s;
	String ltt;
	String lt;
	String c; // change
	String cp;
	String ccol;
	String el;
	String el_cur;
	String elt;
	String ec;
	String ecp;
	String eccol;
	String div;
	String yld;
	
	// Stocks to be monitored
	public static List<HashMap<String,String>> stocks;
    static {
        stocks = new ArrayList<HashMap<String, String>>();
        
        HashMap<String, String> google = new HashMap<String,String>();
        google.put("ticker","GOOG");
        google.put("exchange", "NASDAQ");
        
        HashMap<String, String> microsoft = new HashMap<String,String>();
        microsoft.put("ticker","MSFT");
        microsoft.put("exchange", "NASDAQ");
        
        HashMap<String, String> facebook = new HashMap<String,String>();
        facebook.put("ticker","FB");
        facebook.put("exchange", "NASDAQ");
        
        HashMap<String, String> apple = new HashMap<String,String>();
        apple.put("ticker","AAPL");
        apple.put("exchange", "NASDAQ");
        
        stocks.add(google);
        stocks.add(facebook);
        stocks.add(apple);
        stocks.add(microsoft);
    }
	
	// SharedPreferences are used to store the vaules
    public static String preferences = "stock_prefs";
    SharedPreferences p;	
    static Activity activity;
    
	public static Stock getStock(String exchange, String ticker) throws ClientProtocolException, IOException, JsonSyntaxException, InterruptedException, ExecutionException{
		
		class RequestTask extends AsyncTask<String, String, String>{

		    @Override
		    protected String doInBackground(String... uri) {
		    	StringBuilder builder = new StringBuilder();
		        HttpClient client = new DefaultHttpClient();
		        HttpGet httpGet = new HttpGet(uri[0]);
		        try {
		          HttpResponse response = client.execute(httpGet);
		          StatusLine statusLine = response.getStatusLine();
		          int statusCode = statusLine.getStatusCode();
		          if (statusCode == 200) {
		            HttpEntity entity = response.getEntity();
		            InputStream content = entity.getContent();
		            BufferedReader reader = new BufferedReader(new InputStreamReader(content));
		            String line;
		            while ((line = reader.readLine()) != null) {
		              builder.append(line);
		            }
		          } else {
		            Log.e(Scott.TAG, "Failed to download file");
		          }
		        } catch (ClientProtocolException e) {
		          e.printStackTrace();
		        } catch (IOException e) {
		          e.printStackTrace();
		        }
		        return builder.toString();
		    }

		    @Override
		    protected void onPostExecute(String result) {
		        super.onPostExecute(result);
		    }
		}
		
		Gson gson = new Gson();
		String response = new RequestTask().execute("http://finance.google.com/finance/info?client=ig&q=" + exchange + "%3a" + ticker).get();
		response = response.substring(4, response.length() - 1);
		Log.d(Scott.TAG, "Google here " + response);
		return gson.fromJson(response,Stock.class);

	}

	public void startMonitor(Activity activity) {
		
	    //boolean firstRun = p.getBoolean(BinaryLog.PREFERENCE_FIRST_RUN, true);
		Stock.activity = activity; 
		p = activity.getSharedPreferences(Stock.preferences, activity.MODE_PRIVATE);
		
		for(HashMap<String, String> stock : stocks) {
			p.edit().putString(stock.get("ticker"), stock.get("ticker")).commit();		
			Log.d("SCOTT", "Here");
		}
		Log.d("SCOTT", "Done");
		
		new Thread (new Runnable() {

		    private final Handler mHandler = new Handler();

		    @Override
		    public void run() {
				
				for(HashMap<String, String> stock : stocks) {
					String text = "";
					Stock s = new Stock();
					try {
						s = Stock.getStock(stock.get("exchange"), stock.get("ticker"));
					} catch (JsonSyntaxException e) {
						e.printStackTrace();
					} catch (ClientProtocolException e) {
						e.printStackTrace();
					} catch (IOException e) {
						e.printStackTrace();
					} catch (InterruptedException e) {
						e.printStackTrace();
					} catch (ExecutionException e) {
						e.printStackTrace();
					}
					
					text = stock.get("ticker") + " -> " + s.getValue();
					p.edit().putString(stock.get("ticker"), text).commit();
					
				}
				
				//mHandler.postDelayed(this, 5000);       
				
			}
			
		}).start();
		
	}
	
	public static String getTextForTicker(String ticker) {
		SharedPreferences p = Stock.activity.getApplicationContext()
				.getSharedPreferences(Stock.preferences, 
						Scott.getAppContext().MODE_PRIVATE);
		return p.getString(ticker, "Nothing here");
	}
	
	
	
	public String getValue(){
		return "Ticker Text Goes here";
	}

}
